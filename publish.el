;; publish.el --- Publish org-mode project on Gitlab Pages
;; Author: Stany

;; =======================================================================
;; org-mode
;;  Project publishing: https://orgmode.org/manual/Publishing.html
;;
;; Folders structures:
;;   https://medium.com/@nmayurashok/file-and-folder-structure-for-web-development-8c5c83810a5
;;
;; Themes:
;;    http://www.pirilampo.org/sitemap.html
;;    https://github.com/fniessen/org-html-themes
;;
;;    https://opensource.com/article/20/3/blog-emacs
;;
;;    Theme soft link in publish dir: mklink /D themes <dirname\orgproj\org\.emacs.d\org-themes>
;;

(require 'package)
(package-initialize)
(unless package-archive-contents
  (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
  (package-refresh-contents))
(dolist (pkg '(htmlize))
  (unless (package-installed-p pkg)
    (package-install pkg)))

(require 'ox-publish)

(setq org-publish-project-alist
 			'(
				;; gitlab blog ================
				("site-org"
             :base-directory "./org"
             :base-extension "org"
             :recursive t
             :publishing-function org-html-publish-to-html
             :publishing-directory "./public"
						 :headline-levels 4
						 :exclude "^org-templates"
             ;;:auto-sitemap t
             ;;:sitemap-filename "index.org"
             ;;:sitemap-file-entry-format "%d *%t*"
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.ico\"/>"
						 :section-numbers nil
             ;;:sitemap-style 'list
             ;;:sitemap-sort-files anti-chronologically 
						 :makeindex t
						 )
 				;; The static component just copies files
				("site-static"
             :base-directory "./org"
             ;;:base-extension site-attachments
             :publishing-directory "./public"
						 :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|otf\\|txt\\|pcapng"
						 :exclude "^post/"
						 :exclude "^org-templates"
             :publishing-function org-publish-attachment
             :recursive t)
       ("site" :components ("site-org"))
))
