#+SETUPFILE: ../../org-templates/level-2.org

#+TITLE: Entraînement Stan

#+STARTUP: nologdone
#+STARTUP: noindent
#+STARTUP: constSI
#+STARTUP: align
#+OPTIONS: toc:t num:nil H:3 ^:nil pri:t

#+TODO: AFAIRE ACTUEL | C-FAIT CHANGE PAUSE ANNULE ASUIVRE
#+EXCLUDE_TAGS: noexport vacances

# Local Variables:
# after-save-hook: (lambda nil (when (org-html-export-to-html) (rename-file "EntrainementStan.html" "index.html" t)))
# End


* ACTUEL Entraînements                                             :noexport:
** Base                                                            :noexport:
*** Semaine 50 *s4 r15.*  Récupération (Recovery)
		Proprioception: faire rouler la route

**** <2018-12-11 mar.> Sortie base: 8k + Drills 
		 + Base: 4:58..4:28
		 + Drills: 
			 + jambes tendues 0:20
			 + monter genoux 0:20 

**** <2018-12-12 Wed> Côtes
		 Streching dynamique, echauf 3k jog (5:03), côtes 6x 1:00(@ mile)[r 2:00], 3k jog (5:03)

**** <2018-12-13 Thu 12:00> Sortie base 8k 
		 Sortie Base 8k:(5:02..4:32) ou gym sans impact ou 20:00, 30:00 cardio allure récup

**** <2018-12-13 Thu 18:00> Travail Résistance, gainage 
		 + 1x /(
			 - squats 1 jambe
			 - oblique sur le côté
			 - abdos avec jambes levée
			 - abdos a 4 pattes)/
		 + 2x /(
			 + abdo ciseau jambe sur le côté
			 + abdo sur le dos genous vers poitrine 
			 + adbo sur genou et bras vers le ciel 
			 + abdo sur le dos ouverture jambes 
			 + squat saut sur 1 jambe + saut accroupi)/ 


**** <2018-12-14 Fri 12:00> Fartlek 8k (6x 1:00/1:00) 
		 Streching dynamique, Base: (5:02..4:32) avec 3x 1:00(@ 3:23)[r 1:00]

**** <2018-12-14 Fri 18:00> Jog ou cross training (Optionnel)
		 20-60 min. jog ou cross training, allure récup.


** C-FAIT Build 1
*** Semaine 51 *s5 r14.* 
		Proprioception: oscillation minimale - comme sur une trotte ou courir sous un plafond bas 
		- niveau 26
**** Pause                                                                                      <2018-12-17 Mon> 
**** Sortie base: 11k + Drills                                                                  <2018-12-18 Tue> 
		 Base: 5:02..4:32 + Drills: - monter genoux 2x 0:20 - bonds 2x 0:20
**** Optionnel: Jog ou cross training                                                           <2018-12-18 Tue>
		 20-60 min. jog ou cross training, allure récup
**** Intervalles 10x 400m (@ 3k)[3min. rec active]                                              <2018-12-19 Wed> 
		 Streching dynamique, echauf 2k jog, x 400m (3:36 @ 3K), 2k jog
**** Optionnel: Jog ou cross training                                                           <2018-12-19 Wed>
		 20-60 min. jog ou cross training, allure récup
**** Sortie recup/base 6-12k                                                                    <2018-12-20 Thu> 
		 Sortie Recu:(5:46..5:03) / Sortie Base:(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** Travail Résistance, gainage                                                                <2018-12-20 Thu>
		 PPG: + 2x: - squat 1 jambe - oblique sur le côté - abdos jambe levée s/hanche - abd à 4 pattes + 1x: - abdo ciseau s/côté - hanche genou s/poitrine - adbo s/genou et bras tendus - abdo s/dos ouverture jambes - squat saut s/1 jambe - saut accroupi
**** Intervalles 6x 1k (@ 10k)[2min. rec active]                                                <2018-12-21 Fri>
		 Streching dynamique, echauf 2k jog, x 1k (3:52 @ 10K), 2k jog
**** Optionnel: Jog ou cross training                                                           <2018-12-21 Fri>
		 20-60 min. jog ou cross training, allure récup
**** Sortie recup/base 6-12k                                                                    <2018-12-22 Sat>
		 Sortie Recu:(5:46..5:03) / Sortie Base:(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** Travail Résistance, gainage                                                                <2018-12-22 Sat>
		 PPG: + 2x: - squat 1 jambe - oblique sur le côté - abdos jambe levée s/hanche - abd à 4 pattes + 1x: + abdo ciseau s/côté - hanche genou s/poitrine - adbo s/genou et bras tendus - abdo s/dos ouverture jambes - saut f/mur bras tendus - saut 1 jambe s/banc 40cm
**** Sortie endurance: 16k                                                                      <2018-12-23 Sun>
		 Sortie Base:(5:02..4:32)
**** Optionnel: Jog ou cross training                                                           <2018-12-23 Sun>
		 20-60 min. jog ou cross training, allure récup


*** C-FAIT Semaine 52 *s6 r13.*
		Proprioception: contact minimal au sol - forte poussée
		 - niveau 26
**** Pause                                                   <2018-12-24 Mon> 
**** Sortie base: 12k + Drills                               <2018-12-25 Tue> 
		 Base: 5:02..4:32 + Drills: - jambes tendues 2x 0:20 - sans bras 2x 0:20
**** Intervalles 12x 400m (@ 3k)[3min. rec active]           <2018-12-26 Wed>
		 Streching dynamique, echauf 2k jog, x 400m (3:36 @ 3K), 2k jog
**** Sortie recup/base 6-12k                                 <2018-12-27 Thu> 
		 Sortie Recup:(5:46..5:03) / Sortie Base:(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** Travail Résistance, gainage                             <2018-12-27 Thu>
     + 3x:
			 - squat 1 jambe - oblique sur le côté
			 - abdos jambe levée s/hanche - abdos à 4 pattes
		 + 2x: - saut squat tendu - saut squat fentes
**** Intervalles 7x 1k (@ 10k)[2min. rec active]             <2018-12-28 Fri> 
		 Streching dynamique, echauf 2k jog, x 1k (3:52 @ 10K), 2k jog
**** Optionnel: Jog ou cross training                        <2018-12-28 Fri> 
		 20-60 min. jog ou cross training, allure récup
**** Sortie recup/base 6-12k                                 <2018-12-29 Sat> 
		 Sortie Recu:(5:46..5:03) / Sortie Base:(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** Travail Résistance, gainage                             <2018-12-29 Sat> 
		 + 3x:
			 - squat 1 jambe - oblique sur le côté
			 - abdos jambe levée s/hanche - abdos ä 4 pattes 
		 + 2x: - squat saut s/1 jambe

**** Sortie endurance: 18k                                   <2018-12-30 Sun>
		 Sortie Base:(5:02..4:32)


*** C-FAIT Semaine 1 *s7 r12.*
		niveau 26

		Proprioception: cuisses hautes (exagérer mouvement cuisses & bras)
**** <2018-12-31 Mon> Pause
**** <2019-01-01 Tue> Sortie base: 13k + Drills
		 Base(5:02..4:32) + Drills: - 2x 0:20(sprints côte) - 2x 0:20(saut /1 jambe)
**** <2019-01-01 Tue> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-01-02 Wed> Intervalles 14x 400m(@ 3k)[3min. rec active]
		 Streching dynamique, echauf 2k jog, x 400m (3:36 @ 3K), 2k jog
**** <2019-01-02 Wed> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-01-03 Thu> Sortie recup/base 6-12k
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-01-03 Thu> Travail Résistance, gainage
		 + 2x: - squat 1 jambe - oblique sur le côté - abdos jambe levée s/hanche
			 - abdos à 4 pattes - saut squat fentes - squat saut s/1 jambe 
		 + 1x: - box lunge - Stability ball leg curl - Forearms to palms bridge - Dead bug
**** <2019-01-04 Fri> Intervalles 8x 1k(@ 10k)[2min. rec active]
		 Streching dynamique, echauf 2k jog, x 1k (3:52 @ 10K), 2k jog
**** <2019-01-04 Fri> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-01-05 Sat> Sortie recup/base 6-12k
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-01-05 Sat> Travail Résistance, gainage
		 + 2x: - squat 1 jambe - oblique sur le côté - abdos jambe levée s/hanche
			 - abdos à 4 pattes - saut 1 jambe s/banc 40cm
		 + 1x: - box lunge - Stability ball leg curl - Forearms to palms bridge - Dead bug
**** <2019-01-06 Sun> Sortie endurance: 19k
		 Sortie Base:(5:02..4:32)


*** C-FAIT Semaine 2 *s8 r11.* Récupération (Recovery)
		niveau 26

    Proprioception: pieds relachés - poussée avec les haut des jambes
**** <2019-01-07 Mon> Pause
**** <2019-01-08 Tue> Sortie base: 10k + Drills
		 Base(5:02..4:32) + Drills: - 0:20(monter genoux) - 0:20(foulée bondissante)
**** <2019-01-08 Tue> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-01-09 Wed> Intervalles 8x 400m(@ 3k)[3min. rec active]
		 Streching dynamique, echauf 2k jog, x 400m (3:36 @ 3K), 2k jog
**** <2019-01-09 Wed> Optionnel: Jog ou cross training 
		 20-60 min. jog ou cross training, allure récup
**** <2019-01-10 Thu> Sortie recup/base 6-12k                     
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-01-10 Thu> Travail Résistance, gainage                 
		 + 1x: - squat 1 jambe - oblique sur le côté - abdos jambe levée s/hanche - abdos à 4 pattes
		 + 1x: - box lunge - Stability ball leg curl - Forearms to palms bridge
			 - Dead bug - saut accroupi - saut f/mur bras tendus
**** <2019-01-11 Fri> Intervalles 4x 1k(@ 5k)[2min. rec active]   
		 Streching dynamique, echauf 2k jog, x 1k (3:44 @ 5K), 2k jog
**** <2019-01-11 Fri> Optionnel: Jog ou cross training            
		 20-60 min. jog ou cross training, allure récup
**** <2019-01-12 Sat> Sortie recup/base 6-12k      
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-01-12 Sat> Travail Résistance, gainage                 
     + 1x: - squat 1 jambe - oblique sur le côté - abdos jambe levée s/hanche - abdos à 4 pattes
		 + 1x: - box lunge - Stability ball leg curl - Forearms to palms bridge
			 - Dead bug - saut f/mur bras tendus - saut 1 jambe s/banc 40cm
**** <2019-01-13 Sun> Sortie endurance: 12k
		 Base(5:02..4:32)
**** <2019-01-13 Sun> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup

** C-FAIT Build 2
*** Semaine 3 *s9 r10.*
		niveau 26
**** Proprioception
		 serrer les fesses et hanches - stabilité des hanches
**** <2019-01-14 Mon> Pause
**** <2019-01-15 Tue> Sortie base: 12k + Drills
		 Base(5:02..4:32) + Drills: - 2x 0:20(jambes tendues) - 2x 0:20(sans bras)
**** <2019-01-15 Tue> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-01-16 Wed> Intervalles 4x 1k(@ 5k)[3min. rec active]
		 Streching dynamique, echauf 2k jog, 1k (3:44 @ 5K), 2k jog
**** <2019-01-16 Wed> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-01-17 Thu> Sortie recup/base 6-12k
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-01-17 Thu> Travail Résistance, gainage
     + 1x: - squat 1 jambe - oblique sur le côté 
			 - abdos jambe levée s/hanche - abdos à 4 pattes
		 + 2x: - box lunge - Stability ball leg curl - Forearms to palms bridge
			 - Dead bug - saut accroupi - saut f/mur bras tendus                 |
**** <2019-01-18 Fri> Intervalles 4x 2k(@ 10k)[2min. rec active]
		 Streching dynamique, echauf 2k jog, 1k (3:52 @ 10K), 2k jog
**** <2019-01-18 Fri> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-01-19 Sat> Sortie recup/base 6-12k
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-01-19 Sat> Travail Résistance, gainage
     + 1x: - squat 1 jambe - oblique sur le côté 
			 - abdos jambe levée s/hanche - abdos à 4 pattes
		 + 2x: - box lunge - Stability ball leg curl - Forearms to palms bridge
			 - Dead bug - saut accroupi - saut 1 jambe s/banc 40cm
**** <2019-01-20 Sun> Sortie endurance: 16k
		 Base(5:02..4:32)
**** <2019-01-20 Sun> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup


*** Semaine 4 *s10 r9.*
		niveau 26
**** Proprioception
		 à la recherche de la symétrie
**** <2019-01-21 Mon> Pause
**** <2019-01-22 Tue> Intervalles 6x 1k(@ 5k)[3min. rec active]
		 Streching dynamique, echauf 2k jog, 1k (3:44 @ 5K), 2k jog
**** <2019-01-22 Tue> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-01-23 Wed> Sortie base: 13k + Drills
		 Base(5:02..4:32) + Drills: - 2x 0:20(jambes tendues) - 2x 0:20(sans bras)
**** <2019-01-23 Wed> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-01-24 Thu> Intervalles 5x 2k(@ 10k)[2min. rec active]
		 Streching dynamique, echauf 2k jog, 1k (3:52 @ 10K), 2k jog
**** <2019-01-24 Thu> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-01-25 Fri> Sortie recup/base 6-12k
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-01-25 Fri> Travail Résistance, gainage
     + 2x: - abdos jambe levée s/hanche - single leg squat 
			 - box lunge - Stability ball leg curl - squat jump - split squat jump
**** <2019-01-26 Sat> Sortie recup/base 6-12k
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-01-26 Sat> Travail Résistance, gainage
     + 2x: Cook hip lift - oblique bridge (s/le côté) - Forearms to palms bridge
			 - Stability ball leg curl - split squat jump - saut squat s/1 jambe
**** <2019-01-27 Sun> Sortie endurance: 18k 
		 Base(5:02..4:32)
**** <2019-01-27 Sun> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup

*** Semaine 5 *s11 r8.*
		niveau 26
**** Proprioception
		 un essieu entre les genoux
**** <2019-01-28 Mon> Pause
**** <2019-01-29 Tue> Sortie base: 14k + Drills
     Base(5:02..4:32) + Drills: - 2x 0:20(sprint côtes) - 2x 0:20(saut s/1 jambe)
**** <2019-01-29 Tue> Optionnel: Jog ou cross training
     20-60 min. jog ou cross training, allure récup

**** <2019-01-30 Wed> Intervalles 6x 1k(@ 5k)[2 min. rec. active]
		 Streching dynamique, echauf 2k jog, 1k (3:44 @ 5K), 2k jog
**** <2019-01-30 Wed> Optionnel: Jog ou cross training
     20-60 min. jog ou cross training, allure récup
**** <2019-01-31 Thu> Sortie recup/base 6-12k
     Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-01-31 Thu> Travail Résistance, gainage
      + 2x: abdos jambe levée s/hanche - single leg squat
				- box lunge - Stability ball leg curl - squat jump - split squat jump
**** <2019-02-01 Fri> Intervalles 5x 2k(@ 10k)[90 sec. rec. active]
		 Streching dynamique, echauf 2k jog, 1k (3:52 @ 10K), 2k jog
**** <2019-02-01 Fri> Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-02-02 Sat> Sortie recup/base 6-12k
     Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-02-02 Sat> Travail Résistance, gainage
    + 2x: Cook hip lift - oblique bridge (s/le côté) - Forearms to palms bridge
			- Stability ball leg curl - split squat jump - saut squat s/1 jambe
**** <2019-02-03 Sun> Sortie progressive: 19k, 3km fin allure semi
		 Start allure jog, augmentant vitesse 1.6k/1.6k. + dernier 3km Half(4:04)
**** <2019-02-03 Sun> Optionnel: Jog ou cross training
     20-60 min. jog ou cross training, allure récup


*** Semaine 6 *s12 r7.* Récupération (Recovery) 
		niveau 26
**** Proprioception
		 courir face à un mur
**** <2019-02-04 Mon>  Pause
**** <2019-02-05 Tue>  Sortie base: 10k + Drills
		 Base(5:02..4:32) + Drills: - 0:20(monter genoux) - 0:20(foulée bondissante)
**** <2019-02-05 Tue>  Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-02-06 Wed>  Intervalles 4x 1k(@ 5k)[3min. rec active]
		 Streching dynamique, echauf 2k jog, 1k (3:44 @ 5K), 2k jog
**** <2019-02-06 Wed>  Optionnel: Jog ou cross training
		 20-60 min. jog ou cross training, allure récup
**** <2019-02-07 Thu>  Sortie recup/base 6-12k
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/ 
**** <2019-02-08 Fri>  Intervalles 3x 2k(@ 10k)[2min. rec. active]
		 Streching dynamique, echauf 2k jog, 2k (3:52 @ 10K), 2k jog
**** <2019-02-09 Sat>  Competition de réglage 8k coupe du vignoble
**** <2019-02-09 Sun>  Sortie recup/base 3k

** AFAIRE Affutage
*** C-FAIT Semaine 7 *s13 r6.*
		niveau 26
**** Proprioception
		 tomber en avant
**** <2019-02-11 Mon>  Pause
**** <2019-02-12 Tue>  Sortie base: 13.5k + Drills
		 Base(5:02..4:32) + Drills: - 2x 0:20(monter genoux) - 2x 0:20(foulée bondissante)
**** <2019-02-13 Wed>  Intervalles Mixtes: 2k, 1.6k, 2x1k, 2x800m [2min. rec active]
     - echauff.: Streching dynamique, 1.6k jog 
     - 2k (4:04 @ Half) - 1.6k (3:52 @ 10K)
		 - 2x 1k (3:44 @ 5K) - 2x 800m (3:36 @ 3K)
     - cool down: 1.6k jog

**** <2019-02-14 Thu>  Sortie recup/base 6-12k
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-02-15 Fri>  Intervalles 3x 3k(@ 10k)[3min. rec active]
		 Streching dynamique, echauf 1.6k jog, 3k (3:52 @ 10K), 1.6k jog
**** <2019-02-16 Sat>  Sortie recup/base 6-12k
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-02-17 Sun>  Sortie allure semi: 3k jog + 8k tempo (4:04) + 3k jog


*** AFAIRE Semaine 8 *s14 r5.*
		niveau 26
**** Proprioception
		 rentrer le nombril (active les abdos profonds)
**** <2019-02-18 Mon>  Pause

**** <2019-02-19 Tue>  Sortie base: 14.5k + Drills
		 Base(5:02..4:32) + Drills: - 2x 0:20(jambes tendues) - 2x 0:20(sans bras)
**** <2019-02-20 Wed>  Intervalles Mixtes: 2k, 2x 1.6k, 2x 1k, 2x 800m [2min. rec active]
     - echauff.: Streching dynamique, 1.6k jog 
		 - 2k (4:04 @ Half) - 2x 1.6k (3:52 @ 10K) 
		 - 2x1k (3:44 @ 5K) - 2x800m (3:36 @ 3K)
		 - cool down: 1.6k jog
**** <2019-02-21 Thu>  Sortie recup/base 6-12k
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-02-22 Fri>  Intervalles 4x 3k(@ 10k)[2min. rec active]
		 Streching dynamique, echauf 1.6k jog, 3k (3:52 @ 10K), 1.6k jog
**** <2019-02-23 Sat>  Sortie recup/base 6-12k
		 Recup(5:46..5:03) / Base(5:02..4:32) /Faire recup si la veille on n'a pas fait de recup, sinon faire une sortie de base/
**** <2019-02-24 Sun>  Sortie allure semi: 3k jog + 10k tempo (4:04) + 3k jog

* Compétitions                                                     :noexport:

** 10 km Payerne <2019-03-03 dim.>
	 [[http://www.cabroyard.ch/fr/10km-de-payerne.html][10 Km Payerne]]  dim. 11h
	 


